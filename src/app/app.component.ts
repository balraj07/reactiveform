import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'form';
  signupForm: FormGroup;

  ngOnInit(){
    this.signupForm = new FormGroup({
      'first_name' : new FormControl(null),
      'last_name' : new FormControl(null),
      'email' : new FormControl(null),
      'contact_number' : new FormControl(null),
      'gender' : new FormControl(null),
      'employee_id' : new FormControl(null),
      'password' : new FormControl(null),
      'confirm_password' : new FormControl(null)
    });

  }
  
  onSubmit(){
    console.log(this.signupForm);
  }
}
